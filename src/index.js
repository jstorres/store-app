import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route,} from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.min.css';
import LayoutAdmin from './layouts/LayoutAdmin'
import App from './App';
import './styles/app.css';
import Products from "./routes/products";
import Product from "./routes/product";
import Sales from "./routes/sales";
import Cart from "./routes/cart";
import ThemeProvider from 'react-bootstrap/ThemeProvider'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <BrowserRouter>
            <ThemeProvider
                breakpoints={['xxxl', 'xxl', 'xl', 'lg', 'md', 'sm', 'xs', 'xxs']}
            >
                <LayoutAdmin>
                    <Routes>
                        <Route path="/" element={<App />} />
                        <Route path="sales" element={<Sales />} />
                        <Route path="cart/:id" element={<Cart />} />
                        <Route path="products" element={<Products />} />
                        <Route path="product/:id" element={<Product />} />
                        <Route
                            path="*"
                            element={
                                <main style={{ padding: "1rem" }}>
                                    <p className='text-center text-light'>There's nothing here!</p>
                                </main>
                            }
                        />
                    </Routes>
                </LayoutAdmin>
            </ThemeProvider>;
        </BrowserRouter>,
    </React.StrictMode>
);
reportWebVitals();
