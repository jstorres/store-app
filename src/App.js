import ListProducts from './components/ListProducts'
import products from './data/products.json'


export default function App() {
    return (
    <main style={{ padding: "1rem 0" }}>
        <h2 className="font-weight-bold text-light">Available products</h2>
        <hr />
        <ListProducts products={products} button='buy'></ListProducts>
    </main>
    );
}