import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import logo from '../assets/images/logo.svg';

class ShopCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            validated: false,
            setValidated: false,
            product: {
                id: 1,
                name: "",
                url: "",
                quantity: 0,
                price_unit: 0,
                created_at: "2022-01-11",
                updated_at: "2022-01-11",
                user_created: 0,
                user_edited: 0
            }
        };
    }

    componentDidMount() {
        if(this.props.product) {
            this.setState({
                product: this.props.product,
            });
        }
    }

    ItemProduct = ()  => {
        return (
            <ListGroup.Item
                as="li"
            >
                <Row className="d-flex justify-content-between align-items-start">
                    <Col md="2">
                        <img
                            src={logo}
                            alt={this.state.product.name}
                            style={{
                                width: '110px',
                                height: '110px',
                            }} 
                        />
                    </Col>
                    <Col md="7" className='mt-2 mb-2'>
                        <h3 className="text-left text-capitalize">{this.state.product.name}</h3>
                        <p className='font-italic text-muted mb-0 small'>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            Cupiditate eius tempora quas dolores nobis fuga nisi
                        </p>
                    </Col>
                    <Col md="3">
                        <div
                            className="d-flex justify-content-center align-items-center"
                            style={{
                                height: '80px'
                            }}
                        >
                            <p className="mx-2 mt-2">Qty: 0</p>
                            <Button variant="primary" className='mx-2'>
                                -
                            </Button>
                            <Button variant="danger">
                                +
                            </Button>
                        </div>
                    </Col>
                </Row>
            </ListGroup.Item>
        )
    }

    handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        this.setState({
            validated: true,
        });
    };

    render() {
        return (
            <Row className="d-flex justify-content-between align-items-start">
                    <Col md="8">
                        <ListGroup as="ol">
                            {this.ItemProduct()}
                        </ListGroup>
                    </Col>
                    <Col md="4">
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Title className='h2 font-weight-bold'>Products on cart</Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">Information of shop</Card.Subtitle>
                                <Card.Text>
                                    Some quick example text to build on the card title and make up the bulk of
                                    the card's content.
                                </Card.Text>
                                <small className='text-left text-capitalize'>
                                    Total Items
                                </small>
                                <h4 className='font-weight-bold'>5</h4>
                                <small className='text-left text-capitalize'>
                                    Total Items
                                </small>
                                <h2 className='font-weight-bold'>$ 115.00</h2>
                            </Card.Body>
                            <Card.Footer className="text-capitalize text-muted">max days <b>2</b> to claims</Card.Footer>
                        </Card>
                    </Col>
            </Row>
        )
    }
}
export default ShopCard
