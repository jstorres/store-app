
import React from 'react';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Pagination from 'react-bootstrap/Pagination'
import logo from '../assets/images/logo.svg';
// import { Link } from "react-router-dom";

const perPage = 6

class PaginationBasic extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            total: 0,
            active: 1,
        };
    }

    componentDidMount() {
        const total = (this.props.items.length / perPage)
        this.setState({
            total: total
        });
    }

    handleChange(number) {
        const newActive = number
        this.setState({
            active: newActive
        });
        this.props.onActiveChange(number);
    }

    listButtons() {
        let buttons = []
        for (let number = 1; number <= this.state.total; number++) {
            buttons.push(
                <Pagination.Item
                    key={number}
                    active={number === this.state.active}
                    onClick={(event) => this.handleChange(number)}
                    value={number}
                >
                    {number}
                </Pagination.Item>,
            );
        }
        return buttons
    }

    render() {
        return (
            <div className='d-flex justify-content-center'>
                <Pagination size="sm">{this.listButtons()}</Pagination>
            </div>
        )
    }
}

class ListProductsMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 1,
        };
    }

    listProducts () {
        const itemsPerPage = perPage
        let start = this.props.active === 1 ? 0 * perPage : this.props.active * perPage - perPage
        let end = start + itemsPerPage
        let listProducts = this.props.products.slice(start, end).map(product =>
            <Col 
                key={product.id}
                sm="6" 
                md="4" 
                lg="4" 
                className='pb-1 mr-1'
            >
                <Card className='shadow'>
                    <Card.Header>
                        <Button variant="outline-light text-danger">Delete</Button>
                    </Card.Header>
                    <div className='d-flex justify-content-center'>
                        <Card.Img
                            variant="top"
                            src={logo}
                            style={{
                                width: '120px',
                                height: '120px',
                            }} 
                        />
                    </div>
                    <Card.Body>
                        <Card.Title className='text-center text-capitalize'>{product.name} / {product.id}</Card.Title>
                        <Card.Text className='font-italic text-muted mb-0 small'>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            Cupiditate eius tempora quas dolores nobis fuga nisi
                        </Card.Text>
                        <div className="d-grid gap-2">
                            <small>
                                <b>Stock</b> {product.quantity}
                            </small>
                            <h6 className='font-weight-bold'>
                                $ {product.price_unit}.00
                            </h6>
                            <small>
                                <b>Realese</b> {product.created_at}
                            </small>
                            {this.props.button === 'buy' &&
                                <Button
                                    variant="danger"
                                    block="true"
                                    href={`/cart/${product.id}`}
                                >
                                    Buy
                                </Button>
                            }
                            {this.props.button === 'edit' &&
                                <Button
                                    variant="primary"
                                    block="true"
                                    href={`/product/${product.id}`}
                                >
                                    Edit Product #{product.id}
                                </Button>}
                        </div>
                    </Card.Body>
                </Card>
            </Col>
        )
        return listProducts
    }

    render() {
        return (
            <Row>{this.listProducts()}</Row>
        )
    }
}

class ListProducts extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            active: 1,
        }
    }
    onActiveChangeHandled(newActive) {
        const active = newActive
        this.setState({
            active: active
        });
    }

    render() {
        return (
            <main>
                <div className='mb-2'>
                    <ListProductsMap
                        products={this.props.products.results}
                        active={this.state.active}
                        button={this.props.button}
                    >
                    </ListProductsMap>
                </div>
                <PaginationBasic
                    items={this.props.products.results}
                    onActiveChange={(newActive) => this.onActiveChangeHandled(newActive)}
                >

                </PaginationBasic>
            </main>
        )
    }
}

export default ListProducts