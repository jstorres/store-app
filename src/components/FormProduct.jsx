import React from 'react';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

class FormProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            validated: false,
            setValidated: false,
            product: {
                id: 1,
                name: "",
                url: "",
                quantity: 0,
                price_unit: 0,
                created_at: "2022-01-11",
                updated_at: "2022-01-11",
                user_created: 0,
                user_edited: 0
            }
        };
    }

    componentDidMount() {
        if(this.props.product) {
            console.log(this.props.product, 'this.props.product');
            this.setState({
                product: this.props.product,
            });
        }
    }

    handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        this.setState({
            validated: true,
        });
    };

    render() {
        return (
            <Form
                noValidate
                validated={this.state.validated}
                onSubmit={this.handleSubmit}
            >
                <Row className="mb-3">
                    <Form.Group as={Col} md="12" controlId="validationCustom01">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Name"
                            defaultValue={this.state.product.name}
                            required
                        />
                        <Form.Control.Feedback>Valid!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                                Please enter a product name.
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustom02">
                        <Form.Label>Stock</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Stock"
                            defaultValue={this.state.product.stock}
                            required
                        />
                        <Form.Control.Feedback>Valid!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">
                                Please enter a stock.
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustom03">
                        <Form.Label>Price</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Price"
                            defaultValue={this.state.product.price_unit}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            Please choose a price.
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustom04">
                        <Form.Label>Realese Date</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Realese Date"
                            defaultValue={this.state.product.created_at}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            Please choose a Realese Date.
                        </Form.Control.Feedback>
                    </Form.Group>
                </Row>
                <Button type="submit">
                    {this.props.type === 'create' && 'Save new product'}
                    {this.props.type === 'edit' && 'Save Changes'}
                    {typeof this.props.type === 'undefined' && 'Submit'}
                </Button>
            </Form>
          );
    }
}
export default FormProduct