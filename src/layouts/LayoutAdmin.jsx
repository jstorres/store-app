import { Navbar, Container, Nav } from 'react-bootstrap'

export default function LayoutAdmin(props) {
    return (
        <main>
            <Navbar className="shadow" expand="lg">
                <Container>
                    <Navbar.Brand href="/" className='text-light'>Store-App</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href='/sales' className='text-light'>
                            Sales
                        </Nav.Link>
                        <Nav.Link href='/products' className='text-light'>
                            Products
                        </Nav.Link>
                    </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Container>
                {props.children}
            </Container>
        </main>
    );
  }