import { useParams } from "react-router-dom";
import FormProduct from '../components/FormProduct'
import Card from 'react-bootstrap/Card'
import products from '../data/products.json'
  

const Product = () => {
    let params = useParams();

    const getProduct = (params) => {
        const product = products.results.find(product => parseInt(product.id) === parseInt(params.id))
        return product
    }

    return (
        <>
            <main style={{ padding: "1rem 0" }}>
                <h2 className="font-weight-bold text-light">Product</h2>
                <hr />
                <div className='d-flex justify-content-center'>
                    <Card>
                        <Card.Subtitle className="mx-3 mt-3">
                            Edit Product
                        </Card.Subtitle>
                        <Card.Body>
                            <FormProduct
                                type='edit'
                                product={getProduct(params)}
                            >
                            </FormProduct>
                        </Card.Body>
                    </Card>
                </div>
            </main>
        </>
    )
}
export default Product