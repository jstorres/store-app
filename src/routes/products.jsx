import ListProducts from '../components/ListProducts'
import products from '../data/products.json'


export default function Products() {
    return (
      <main style={{ padding: "1rem 0" }}>
        <h2 className="font-weight-bold text-light">Products</h2>
        <hr />
        <ListProducts
            products={products}
            button="edit"
        >
        </ListProducts>
      </main>
    );
  }