import { useParams } from "react-router-dom";
import ShopCard from '../components/ShopCard'
import products from '../data/products.json'

export default function Cart() {
    let params = useParams();

    const getProduct = (params) => {
        const product = products.results.find(product => parseInt(product.id) === parseInt(params.id))
        return product
    }
    return (
        <main style={{ padding: "1rem 0" }} s>
            <h2 className="font-weight-bold text-light text-center">STORE</h2>
            <h6 className="text-muted text-center">This is the Store Page.</h6>
            <hr />
            <ShopCard product={getProduct(params)}></ShopCard>
        </main>
    );
}